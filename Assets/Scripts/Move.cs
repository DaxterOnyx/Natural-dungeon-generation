using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Move : MonoBehaviour
{
    public new Transform camera;
    public float mouseSensitivity = 100f;
    public float speed = 3f;

    private float _xRot = 0;
    private float _yRot = 0;
    private bool _forward = false;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        _xRot = -y * Time.deltaTime * mouseSensitivity;
        _yRot = x * Time.deltaTime * mouseSensitivity;
        // camera.localRotation = Quaternion.Euler(Mathf.Clamp(_xRot, -90f, 90f), _yRot, 0f);
        camera.Rotate(new Vector3(0f, _yRot, 0f), Space.World);
        camera.Rotate(new Vector3(Mathf.Clamp(_xRot, -90f, 90f), 0f, 0f), Space.Self);
        camera.position = transform.position;

        var rb = GetComponent<Rigidbody>();
        var velocity = rb.velocity;
        if (Input.GetKey(KeyCode.Z))
        {
            rb.AddForce(camera.rotation * new Vector3(0, 0, speed * Time.deltaTime), ForceMode.VelocityChange);
        }
    }
}