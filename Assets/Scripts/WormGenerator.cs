using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using Random = UnityEngine.Random;

namespace WormGenerator
{
    public class WormGenerator : MonoBehaviour
    {
        public float radius = 10;
        public int nbLevel = 1;
        public bool randomSeed = true;
        public int seed;

        public int nbWormAtStart = 5;
        public float life = 10;
        public float speed = 1;
        public Vector2 rotation = new Vector2(33, 15);
        public float reproduction = 1f;
        public GameObject prefab;
        public int nbSexion = 8;
        
        private List<Level> dungeon;


        struct Level
        {
            public List<Path> paths;

            public Level(List<Path> paths)
            {
                this.paths = paths;
            }
        }

        public struct Path
        {
            public List<Vector3> positions;
            public List<Vector3> directions;
            public List<float> dimensions;
            public Color color;
            public List<Path> childs;

            public Path(Vector3 firstPos, Vector3 firstDir, float firstDim)
            {
                positions = new List<Vector3>();
                positions.Add(firstPos);
                color = new Color(Random.value, Random.value, Random.value);
                directions = new List<Vector3>();
                directions.Add(firstDir);
                childs = new List<Path>();
                dimensions = new List<float>();
                dimensions.Add(firstDim);
            }
        }


        public void Generate()
        {
            dungeon = new List<Level>(nbLevel);
            if (randomSeed)
                seed = Random.Range(int.MinValue + 1, int.MaxValue - 1);
            Random.InitState(seed);

            dungeon.Add(new Level(new List<Path>()));

            var angle = 360f / nbWormAtStart;
            var dir = Vector3.forward;
            for (int i = 0; i < nbWormAtStart; i++)
            {
                Worm worm = new Worm(Vector3.zero+dir, dir, 1);

                dungeon[0].paths.Add(worm.CreatePath(life, speed, rotation.x, rotation.y, reproduction));
                dir = Quaternion.Euler(0, angle, 0) * dir;
            }

            Draw();
        }

        private void Draw()
        {
            if (dungeon == null) return;
            Clean();
            foreach (var level in dungeon)
            {
                foreach (var path in level.paths)
                {
                    DrawPath(path).parent = transform;
                }
            }
        }

        private Transform DrawPath(Path path)
        {
            var go = new GameObject("path", typeof(MeshFilter), typeof(MeshRenderer));
            var meshFilter = go.GetComponent<MeshFilter>();
            meshFilter.mesh = CreatePath(path);
            var material = new Material(prefab.GetComponent<MeshRenderer>().sharedMaterial)
                {color = path.color};
            go.GetComponent<MeshRenderer>().material = material;
            var t = go.AddComponent<MeshCollider>();

            foreach (Path child in path.childs)
            {
                DrawPath(child).parent = go.transform;
            }

            return go.transform;
        }

        private void Clean()
        {
            while (transform.childCount > 0)
            {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }

        private Mesh CreatePath(Path path)
        {
            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();
            int nbVertices = 0;
            for (int i = 0; i < path.positions.Count - 1; i++)
            {
                Vector3 pos = path.positions[i];
                Vector3 dir = path.directions[i];
                float dim = path.dimensions[i];
                Vector3 nextPos = path.positions[i + 1];
                Vector3 nextDir = path.directions[i + 1];
                float nextDim = path.dimensions[i + 1];

                for (int sectionIndex = 0; sectionIndex < nbSexion; sectionIndex++)
                {
                    CreateTriangle(vertices, triangles, nbVertices, sectionIndex, pos, dir, dim, nextPos, nextDir,
                        nextDim);

                    nbVertices += 6;
                }
            }

            Mesh mesh = new Mesh {name = "PathMesh", vertices = vertices.ToArray(), triangles = triangles.ToArray()};
            mesh.RecalculateNormals();
            return mesh;
        }

        private void CreateTriangle(List<Vector3> vertices, List<int> triangles, int verticeIndex,
            int sectionIndex, Vector3 pos, Vector3 dir, float dim,
            Vector3 nextPos, Vector3 nextDir, float nextDim)
        {
            var newVertice = VertexPos(pos, dir, sectionIndex, dim);
            var nextNewVertice = VertexPos(nextPos, nextDir, sectionIndex + 1, nextDim);

            //TRIANGLE 1
            vertices.Add(newVertice);
            triangles.Add(verticeIndex);
            vertices.Add(VertexPos(nextPos, nextDir, sectionIndex, nextDim));
            triangles.Add(verticeIndex + 1);
            vertices.Add(nextNewVertice);
            triangles.Add(verticeIndex + 2);

            //TRIANGLE 2
            vertices.Add(newVertice);
            triangles.Add(verticeIndex + 3);
            vertices.Add(nextNewVertice);
            triangles.Add(verticeIndex + 4);
            vertices.Add(VertexPos(pos, dir, sectionIndex + 1, dim));
            triangles.Add(verticeIndex + 5);
        }

        private Vector3 VertexPos(Vector3 pos, Vector3 dir, int sectionIndex, float dim)
        {
            Vector3 delta = Vector3.up * dim;
            return pos + Quaternion.AngleAxis((360f / nbSexion) * sectionIndex, dir) * delta;
        }
    }
}