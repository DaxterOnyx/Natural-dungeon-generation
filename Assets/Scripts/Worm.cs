using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

namespace WormGenerator
{
    public class Worm
    {
        private Vector3 _position;
        private Vector3 _direction;
        private float _dimension;
        private WormGenerator.Path _path;

        public Worm(Vector3 position, Vector3 direction, float dimension)
        {
            _position = position;
            _direction = direction;
            _dimension = dimension;
            _path = new WormGenerator.Path();
        }

        public WormGenerator.Path CreatePath(float life, float speed, float horizontalRotation,
            float verticalRotation,
            float reproduction)
        {
            _path = new WormGenerator.Path(_position, _direction, _dimension);
            while (life > 0)
            {
                //Move forward
                _position += _direction * (1 + NormalRandom()) * speed * (_dimension / 2f);

                life--;
                //Reproduction
                bool rep = Random.value * 100f <= reproduction;
                if (rep && life > 0)
                {
                    var nbChild = Random.Range(2, 10);

                    float rotByChild = 360f / (nbChild + 1);
                    _direction = Quaternion.Euler(0, (Mathf.CeilToInt((nbChild + 1) / 2f)-1) * rotByChild, 0) *
                                 _direction;
                    for (int i = 0; i < nbChild; i++)
                    {
                        Worm child = new Worm(_position, _direction, _dimension);
                        _path.childs.Add(child.CreatePath(life, speed, horizontalRotation, verticalRotation,
                            reproduction));
                        _direction = Quaternion.Euler(0, -rotByChild, 0) * _direction;
                    }

                    break;
                }

                //Rotate
                float rv = NormalRandom();
                float rh = NormalRandom();
                _direction = Quaternion.Euler(rh * verticalRotation, rv * horizontalRotation, 0) * _direction;
                //Force go down
                if (_direction.y > 0)
                    _direction.y = -_direction.y;
                //Change Dimension
                _dimension += Mathf.Max(NormalRandom() + 0.1f, -0.75f);
                if (_dimension < 0)
                    //DEAD
                    break;
                //_dimension = -_dimension;
                //Save position and direction
                _path.positions.Add(_position);
                _path.directions.Add(_direction);
                _path.dimensions.Add(_dimension);
            }

            if (life <= 0)
            {
                _position += _direction * speed;

                _path.positions.Add(_position);
                _path.directions.Add(_direction);
                _path.dimensions.Add(0);
            }

            return _path;
        }

        private float NormalRandom()
        {
            float u = Random.value;
            float v = Random.value;

            float n = Mathf.Sqrt(-2f * Mathf.Log(u)) * Mathf.Sin(2 * Mathf.PI * v) / 6f;
            return n;
        }
    }
}