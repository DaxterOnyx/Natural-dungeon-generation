using System;
using System.CodeDom.Compiler;
using Unity.Mathematics;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(WormGenerator.WormGenerator))]
public class WormGeneratorEditor : UnityEditor.Editor
{
    private WormGenerator.WormGenerator _inspected;
    private LongField _complexity;

    public override VisualElement CreateInspectorGUI()
    {
        _inspected = (WormGenerator.WormGenerator) target;

        var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/UIEditor/WormGeneratorInspector.uxml");
        var root = visualTree.CloneTree();
        root.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/UIEditor/WormGeneratorInspector.uss"));

        root.Q<Button>("generateDungeon").clicked += _inspected.Generate;

        // root.Q<SliderInt>("nbWorms").RegisterValueChangedCallback(e => OnValueChanged());
        // root.Q<FloatField>("life").RegisterValueChangedCallback(e => OnValueChanged());
        // root.Q<FloatField>("speed").RegisterValueChangedCallback(e => OnValueChanged());
        // root.Q<Slider>("reproduction").RegisterValueChangedCallback(e => OnValueChanged());
        //
        // _complexity = root.Q<LongField>("complexity");
        // OnValueChanged();
        
        return root;
    }

    // void OnValueChanged()
    // {
    //     _complexity.value = (long) (((long)_inspected.nbWormAtStart)*Math.Pow(10,_inspected.life));
    // }
}