using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public int size = 10;

    public string seed;
    public bool useRandomSeed;

    [Range(0, 100)] public int randomFillPercent;

    [Range(0, 100)] public int height = 0;
    [Min(0)] public int smoothPower = 5;

    int[,,] map;

    void Start()
    {
        GenerateMap();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GenerateMap();
        }
    }

    void GenerateMap()
    {
        map = new int[size, size, size];
        RandomFillMap();
        
        for (int i = 0; i < smoothPower; i++)
        {
            SmoothMap();
        }
    }


    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int z = 0; z < size; z++)
                {
                    if (x == 0 || x == size - 1 || y == 0 || y == size - 1)
                    {
                        map[x, y, z] = 1;
                    }
                    else
                    {
                        map[x, y, z] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                    }
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int z = 0; y < size; y++)
                {
                    int neighbourWallTiles = GetSurroundingWallCount(x, y, z);

                    if (neighbourWallTiles < 4)
                        map[x, y, z] = 1;
                    else if (neighbourWallTiles > 4)
                        map[x, y, z] = 0;
                }
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY, int gridZ)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                for (int neighbourZ = gridZ - 1; neighbourZ <= gridZ + 1; neighbourZ++)
                {
                    if (neighbourX >= 0 && neighbourX < size && neighbourY >= 0 && neighbourY < size &&
                        neighbourZ >= 0 && neighbourZ < size)
                    {
                        if (neighbourX != gridX || neighbourY != gridY || neighbourZ != gridZ)
                        {
                            wallCount += map[neighbourX, neighbourY, neighbourZ];
                        }
                    }
                    else
                    {
                        wallCount++;
                    }
                }
            }
        }

        return wallCount;
    }


    void OnDrawGizmos()
    {
        if (map != null)
        {
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    int z = Mathf.Clamp(height,1,size-2);
                    if (map[x, y, z] == 0)
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = new Vector3(-size / 2 + x + .5f, -size / 2 + z + .5f, -size / 2 + y + .5f);
                        Gizmos.DrawCube(pos, Vector3.one);
                    }
                    z--;
                    if (map[x, y, z] == 0)
                    {
                        Gizmos.color = new Color(1,0,0,0.5f);
                        Vector3 pos = new Vector3(-size / 2 + x + .5f, -size / 2 + z + .5f, -size / 2 + y + .5f);
                        Gizmos.DrawCube(pos, Vector3.one);
                    }
                    z+=2;
                    if (map[x, y, z] == 0)
                    {
                        Gizmos.color = new Color(0,0,1,0.5f);
                        Vector3 pos = new Vector3(-size / 2 + x + .5f, -size / 2 + z + .5f, -size / 2 + y + .5f);
                        Gizmos.DrawCube(pos, Vector3.one);
                    }
                }
            }
        }
    }
}